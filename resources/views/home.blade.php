@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <table class="table table-striped table-dark">
                    <thead>
                    <tr>
                        <th scope="col">Task Name</th>
                        <th scope="col">Created by</th>
                        <th scope="col">Assigned to</th>
                        <th scope="col">Status</th>
                        <th scope="col">Do this task</th>
                    </tr>
                    </thead>
                    <tbody>

                    @isset($tasks)
                        @foreach($tasks as $task)
                            <tr>
                                <th scope="row">
                                    <a onclick="document.getElementById('view{{$task['id']}}').submit();"
                                       href="#">{{$task["name"]}}</a>
                                    <form id="view{{$task['id']}}" action="{{route("view_task")}}" method="post">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$task['id']}}">
                                    </form>
                                </th>
                                <td>
                                    @isset($users)
                                        @foreach($users as $user)
                                            @if($user['id']==$task['user_id'])
                                                {{$user['name']}}
                                            @endif
                                        @endforeach
                                    @endisset
                                </td>
                                <td>
                                    @isset($users)
                                        @foreach($users as $user)
                                            @if($user['id']==$task['assigned_to'])
                                                {{$user['name']}}
                                            @endif
                                        @endforeach
                                    @endisset
                                </td>
                                <td>
                                    @if($task["status"]== 0)
                                        New
                                    @elseif($task["status"] == 1)
                                        Pending
                                    @else
                                        Completed
                                    @endif</td>

                                <td>
                                    <form action="{{route("attache_me")}}" method="post">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$task["id"]}}">
                                        <button @if($task['assigned_to'] != 0 ) disabled @endif type="submit"
                                                class="btn btn-primary">do this task
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @endisset
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
