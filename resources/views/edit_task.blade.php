@extends("layouts.app")
@section("content")
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="form">
                    <form action="{{route("update_task")}}" method="post">
                        @csrf
                        @isset($tasks)
                            @foreach($tasks as $task)
                                <input type="hidden" name="id" value="{{$task['id']}}">
                                <div class="form-group">
                                    <label for="name">Task Name</label>
                                    <input class="form-control" name="name" id="name" value="{{$task['name']}}"
                                           type="text">
                                </div>
                                <div class="form-group">
                                    <label for="attached">Assigned to</label>
                                    <select class="form-control" name="assigned_to" id="attached">
                                        <option value="0"></option>
                                        @isset($users)
                                            @foreach($users as $user)
                                                @if($task['Assigned_to'] == $user["id"])
                                                    <option selected value="{{$user["id"]}}">{{$user["name"]}}</option>
                                                @else
                                                    <option value="{{$user["id"]}}">{{$user["name"]}}</option>
                                                @endif

                                            @endforeach
                                        @endisset
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select class="form-control" name="status" id="status">
                                        @if($task['status']==0)
                                            <option value="0" selected>New</option>
                                            <option value="1">Pending</option>
                                            <option value="2">Completed</option>
                                        @elseif($task["status"] == 1)
                                            <option value="0">New</option>
                                            <option value="1" selected>Pending</option>
                                            <option value="2">Completed</option>
                                        @else
                                            <option value="0">New</option>
                                            <option value="1">Pending</option>
                                            <option value="2" selected>Completed</option>

                                        @endif

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="explination">Explination</label>
                                    <input class="form-control" type="text" value="{{$task['explination']}}" name="explination" id="explination">
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            @endforeach
                        @endisset
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
