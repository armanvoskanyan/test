@extends("layouts.app")
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-4">


                <table class="table table-dark">
                    <tr>
                        <td><b>Task Name:</b></td>
                        <td>{{$task['name']}}</td>
                    </tr>
                    <tr>
                        <td><b>Created By:</b></td>
                        <td>@foreach($users as $user)
                                @if($user['id']==$task['user_id'])
                                    {{$user['name']}}
                                @endif

                            @endforeach</td>
                    </tr>
                    <tr>
                        <td><b>Assigned To:</b></td>
                        <td>@foreach($users as $user)
                                @if($user['id']==$task['assigned_to'])
                                    {{$user['name']}}
                                @endif

                            @endforeach</td>

                    </tr>
                    <tr>
                        <td><b>Status:</b></td>
                        <td>@if($task['status']==0)
                                New
                            @elseif($task["status"] == 1)
                                Pending
                            @else
                                Completed
                            @endif</td>
                    </tr>
                    <tr>
                        <td><b>Explination:</b></td>
                        <td>{{$task['explination']}}</td>
                    </tr>
                </table>


            </div>
        </div>
    </div>
@endsection
