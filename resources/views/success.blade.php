@extends("layouts.app")
@section('content')
    <div class="container">
        <div class="row justify-content-center">

            @isset($message)
                    <div class="alert alert-success">{{$message}}</div>
            @endisset</div>
    </div>

@endsection
