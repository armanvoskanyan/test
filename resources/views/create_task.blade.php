@extends("layouts.app")
@section("content")
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @isset($errors)
                @foreach($errors as $error)
                    <div class="alert alert-danger">{{$error}}</div>
                @endforeach
                @endisset
                <div class="form">
                    <form action="{{route("save_task")}}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="name">Task Name</label>
                            <input class="form-control" name="name" id="name" type="text">
                        </div>
                        <div class="form-group">
                            <label for="attached">Assigned to</label>
                            <select class="form-control" name="assigned_to" id="attached">
                                <option value="0"></option>
                                @isset($users)

                                @foreach($users as $user)
                                        <option value="{{$user["id"]}}">{{$user["name"]}}</option>

                                    @endforeach
                                    @endisset
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="status">Status</label>
                            <select class="form-control" name="status" id="status" >
                                <option value="0">New</option>
                                <option value="1">Pending</option>
                                <option value="2">Completed</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="explination">Explination</label>
                            <input class="form-control" type="text" name="explination" id="explination">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
