@extends("layouts.app")
@section("content")
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <table class="table table-striped table-dark">
                    <thead>
                    <tr>
                        <th scope="col">Task Name</th>
                        <th scope="col">Created by</th>
                        <th scope="col" style="width: 200px; display: flex">Status</th>
                        <th scope="col">Edit</th>
                    </tr>
                    </thead>
                    <tbody>

                    @isset($tasks)
                        @foreach($tasks as $task)
                            <tr>
                                <th scope="row"><a onclick="document.getElementById('view{{$task["id"]}}').submit();"
                                                   href="#">{{$task["name"]}}</a>
                                    <form id="view{{$task['id']}}" action="{{route("view_task")}}" method="post">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$task['id']}}">
                                    </form>
                                </th>
                                <td>
                                    @foreach($user as $myuser)
                                        @if($myuser["id"] == $task["user_id"])
                                            {{$myuser["name"]}}
                                        @endif
                                    @endforeach
                                </td>

                                <td>


                                    <select class="form-control" id="status">
                                        @if($task['status']==0)
                                            <option value="0" selected>New</option>
                                            <option value="1">Pending</option>
                                            <option value="2">Completed</option>
                                        @elseif($task["status"] == 1)
                                            <option value="0">New</option>
                                            <option value="1" selected>Pending</option>
                                            <option value="2">Completed</option>
                                        @else
                                            <option value="0">New</option>
                                            <option value="1">Pending</option>
                                            <option value="2" selected>Completed</option>

                                        @endif
                                    </select>

                                </td>
                                <td>
                                    <form action="{{route("update_attached")}}" method="POST">
                                        @csrf
                                        <input type="hidden" name="status">
                                        <script>
                                            $(document).ready(function () {
                                                $("#status").on("change", function () {
                                                    $("input[name='status']").val($(this).val())
                                                })
                                            })
                                        </script>
                                        <input type="hidden" name="id" value="{{$task["id"]}}">
                                        <input type="submit" class="btn btn-primary" value="Edit">
                                    </form>
                                </td>

                            </tr>
                        @endforeach
                    @endisset
                    </tbody>
                </table>
            </div>
        </div>
    </div>














@endsection
