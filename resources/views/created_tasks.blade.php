@extends("layouts.app")
@section("content")


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <table class="table table-striped table-dark">
                    <thead>
                    <tr>
                        <th scope="col">Task Name</th>
                        <th scope="col">Assigned to</th>
                        <th scope="col" style="width: 150px; display: flex;height:72px;">Status</th>
                        <th scope="col">Edit</th>
                    </tr>
                    </thead>
                    <tbody>

                    @isset($tasks)
                        @foreach($tasks as $task)
                            <tr>
                                <th scope="row"><a onclick="document.getElementById('view{{$task['id']}}').submit();"
                                                   href="#">{{$task["name"]}}</a>
                                    <form id="view{{$task["id"]}}" action="{{route("view_task")}}" method="post">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$task['id']}}">
                                    </form>
                                </th>
                                <td>@foreach($users as $user)

                                        @if($user["id"]==$task["assigned_to"])
                                            {{$user["name"]}}
                                        @endif
                                    @endforeach
                                </td>
                                <td>@if($task["status"] == 0)
                                        New
                                    @elseif($task["status"] == 1)
                                        Pending
                                    @else

                                        Completed
                                    @endif</td>
                                <td>
                                    <form action="{{route("edit_task")}}" method="post">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$task["id"]}}">
                                        <button type="submit" class="btn btn-primary">Edit</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @endisset
                    </tbody>
                </table>
            </div>
        </div>
    </div>














@endsection
