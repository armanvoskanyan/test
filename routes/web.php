<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Route;



Auth::routes();

//Route::get('/', 'HomeController@index')->name('home');
Route::get("/created","TaskController@index")->name("created");
Route::get("/add-task","TaskController@create")->name("create_task");
Route::post("/store","TaskController@store")->name("save_task");
Route::get("/attached","TaskController@index1")->name("attached");
Route::post("/edit","TaskController@edit")->name("edit_task");
Route::get("/","LogoffController@index")->name("all_tasks");
Route::post("/update","TaskController@update")->name("update_task");
Route::post("/update_attached","TaskController@updateAttached")->name("update_attached");
Route::post("/attache_me","TaskController@attache")->name("attache_me");
Route::post("/view_task","LogoffController@view")->name("view_task");
Route::get('/search','LogoffController@search')->name('search');
