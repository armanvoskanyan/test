<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use App\Task;
use Validator;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user_id = Auth::user()->id;
        $tasks = Task::where("user_id", $user_id)->get();
        $users = User::all();
        return view("created_tasks", compact("tasks", "users"));
    }

    public function index1()
    {
        $user_id = Auth::user()->id;
        $tasks = Task::where("assigned_to", $user_id)->get();
        $user = User::all();
        return view("attached_tasks", compact("tasks", "user"));
    }

    public function create()
    {
        $users = User::all();
        return view("create_task", compact("users"));
    }

    public function store(Request $request)
    {
        $input = $request->except("_token");
        $validator = Validator::make($input, [
            "name" => "required|max:255",
            "assigned_to" => "required",
        ]);
        $input["user_id"] = Auth::user()->id;
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $tasks = new Task;
        $tasks->fill($input);
        if ($tasks->save()) {
            return view("success")->with('message', "Created Successfully!");
        } else {
            return redirect()->back()->withErrors()->withInput();
        }
    }

    public function edit(Request $request)
    {
        $id = $request["id"];
        $tasks = Task::where("id", $id)->get();
        $users = User::all();
        return view("edit_task", compact("tasks", 'users'));
    }

    public function update(Request $request)
    {
        $input = $request->except("_token");
        $validator = Validator::make($input, [
            "name" => "required|max:255",
            "assigned_to" => "required",
        ]);

        $input["user_id"] = Auth::user()->id;
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $task = Task::find($input['id']);
        if ($task->update($input)) {
            return view("success")->with('message', "Created Successfully!");
        } else {
            return redirect()->back()->withErrors();
        }

    }


    public function attache(Request $request)
    {
        $id = $request['id'];
        $user_id = Auth::user()->id;
        $task = Task::find($id);
        $task['assigned_to'] = $user_id;
        if ($task->update()) {
            return view("success")->with('message', "Assigned Successfully!");
        } else {
            return redirect()->route("all_tasks");
        }
    }

    public function updateAttached(Request $request)
    {
        $id = $request['id'];
        $task = Task::find($id);
        $task['status'] = $request['status'];
        if ($task->update()) {
            return view("success")->with('message', "Status changed Successfully!");
        } else {
            return redirect()->route("all_tasks");
        }
    }

}

