<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use App\Task;
use Validator;
use Illuminate\Http\Request;

class LogoffController extends Controller
{
    public function index()
    {
        $tasks = Task::all();
        $users = User::all();
        return view("home", compact("tasks", 'users'));
    }

    public function view(Request $request)
    {
        $task = Task::where('id', $request['id'])->first();
        $users = User::all();
        return view("view_task", compact('task', 'users'));
    }

    public function search(Request $request)
    {
        $search = $request->search;
        if ($search == '') {
            return redirect()->back();
        } else {
            $tasks = Task::where('name', 'LIKE', $search . "%")->paginate(6);
            $tasks->appends($request->only('search'));
            $users = User::all();
            return view('home', compact('tasks', 'users'));
        }
    }
}
